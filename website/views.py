from django.shortcuts import render, get_object_or_404, redirect
# from django.shortcuts import render_to_response
from django.contrib.auth import authenticate, login
from .custom_sctipt import get_reviews_new_dot, get_booking_request_dot
from datetime import datetime
from .models import *
from .forms import *
from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required


# Create your views here.
def index(request):
    messages = IndexMessage.objects.all()

    return render(request, 'website/index.html', {
        "messages": messages,
    })


def promo(request):
    promos = Promo.objects.all()
    return render(request, 'website/promo.html', {'promos': promos, })


def rooms(request, url=None, url2=None):
    if url is not None:
        r_type = get_object_or_404(RType, url=url)
        r_type_photos = RTypePhoto.objects.filter(r_type=r_type)

        r_type_equipments = RTypeEquipment.objects.filter(r_type=r_type)

        special_rooms = Room.objects.filter(r_type=r_type, show=True)

        price = Price.objects.first()
        price_current = RTypeSeason.objects.filter(r_type=r_type).order_by('season__start_date').first().price
        price_current_two = price_current + price.double_room
        price_extra_guest = price.extra_guest

        return render(request, 'website/room.html', {
            'r_type': r_type,
            'r_type_photos': r_type_photos,
            'price_current': price_current,
            'price_current_two': price_current_two,
            'price_extra_guest': price_extra_guest,
            'r_type_equipments': r_type_equipments,
        })

    r_types = RType.objects.all()
    rooms = []
    for r_type in r_types:  # тип номера и первое изображение для типа
        r_type_photo_first = RTypePhoto.objects.filter(r_type=r_type).first()
        price_low = RTypeSeason.objects.filter(r_type=r_type).order_by('price').first()
        if price_low is not None:
            price_low = price_low.price
        rooms.append([r_type, r_type_photo_first, price_low])
    return render(request, 'website/rooms.html', {
        'rooms': rooms,
    })


def services(request):
    services = Service.objects.all()
    services_photo = ServicePhoto.objects.all()

    services_comb = []

    for service in services:
        services_comb.append([service, services_photo.filter(service__title=service.title).first().photo])

    return render(request, 'website/services.html', {
        'services_comb': services_comb,

    })


def prices(request):
    seasons = Season.objects.all().order_by('start_date')
    rtype_seasons = RTypeSeason.objects.all()

    price = Price.objects.first()

    return render(request, 'website/prices.html', {
        'rtype_seasons': rtype_seasons,
        'seasons': seasons,
        'price': price,
    })


def gallery(request, url=None):
    if url is not None:
        galleries = Gallery.objects.all()
        urls = []

        current_gallery = None
        for gallery in galleries:
            urls.append(gallery.url)
            if gallery.url == url:
                current_gallery = gallery

        gallery_photos = GalleryPhoto.objects.filter(gallery=current_gallery)
        photos = []
        for gallery_photo in gallery_photos:
            photos.append(gallery_photo.photo)

        if url in urls:
            return render(request, 'website/gallery.html', {
                'galleries': galleries,
                'photos': photos,
            })

    first_gallery = Gallery.objects.first()
    return redirect('gallery-open', url=first_gallery.url)


def reviews(request):
    # обаботка формы отпавки отзыва
    if request.method == 'POST':
        review_send_form = ReviewSendForm(request.POST)
        if review_send_form.is_valid():
            review = review_send_form.save()
            return render(request, 'website/forms_responses/review_send_form.html', {'author': review.author})

    reviews_public = Review.objects.filter(status=1)
    review_send_form = ReviewSendForm()
    return render(request, 'website/reviews.html', {
        'reviews_public': reviews_public,
        'review_send_form': review_send_form,
    })


def contacts(request):
    return render(request, 'website/contacts.html')


def booking(request, beg=None, fin=None, id=None):
    if beg is None or fin is None:
        today = datetime.date.today().strftime('%Y-%m-%d')
        return redirect('booking-date', beg=today, fin=today)

    book_date_form = BookingDateForm(initial={'date_in': beg, 'date_out': fin})
    beg_obj = datetime.datetime.strptime(beg, '%Y-%m-%d')
    fin_obj = datetime.datetime.strptime(fin, '%Y-%m-%d')
    if beg_obj > fin_obj:
        fin_obj = beg_obj
        fin = beg
        return redirect('booking-date', beg=beg, fin=fin)

    days = (fin_obj - beg_obj).days

    available_rooms = Room.objects.all()
    open_bookings = list(Booking.objects.filter(is_over=False))

    for open_booking in open_bookings:
        if (open_booking.date_in <= beg_obj.date() and open_booking.date_out <= beg_obj.date()) \
                or (open_booking.date_in >= fin_obj.date() and open_booking.date_out >= fin_obj.date()):
            open_bookings.remove(open_booking)

    avialable_rtypes = dict()

    for avialable_room in available_rooms:
        if avialable_room.r_type.title in avialable_rtypes:
            avialable_rtypes[avialable_room.r_type.title] += 1
        else:
            avialable_rtypes[avialable_room.r_type.title] = 1

    for avialable_rtype in avialable_rtypes:
        if avialable_rtypes[avialable_rtype] > 0:
            for open_booking in open_bookings:
                if open_booking.room.r_type.title == avialable_rtype:
                    avialable_rtypes[avialable_rtype] -= 1

    rtypes = []
    for avialable_rtype in avialable_rtypes:
        if avialable_rtypes[avialable_rtype] > 0:
            rtypes.append(avialable_rtype)

    rtypes_price = []

    for rtype in rtypes:
        r_type_season = RTypeSeason.objects.filter(r_type__title=rtype).order_by('-season__start_date').first()
        rtypes_price.append([rtype, r_type_season.price, r_type_season.r_type.id])

    price = 0
    r_type_season = RTypeSeason.objects.filter(r_type__id=id).order_by('-season__start_date').first()
    if r_type_season is not None:
        price = r_type_season.price * days

    booking_request_form = None
    if id is not None:
        booking_request_form = BookingRequestForm()
        if request.method == 'POST':
            booking_request_form = BookingRequestForm(request.POST)
            if booking_request_form.is_valid():
                booking_request = BookingRequest()
                booking_request.phone = booking_request_form.data['phone']
                booking_request.name = booking_request_form.data['name']
                booking_request.rtype = RType.objects.get(id=id)
                booking_request.date_in = beg
                booking_request.date_out = fin
                booking_request.peoples = booking_request_form.data['peoples']
                booking_request.save()
                return redirect('booking-successful')

    return render(request, 'website/booking.html', {
        'book_date_form': book_date_form,
        'days': days,
        'rtypes_price': rtypes_price,
        'beg': beg,
        'fin': fin,
        'id': id,
        'price': price,
        'booking_request_form': booking_request_form,
    })


def booking_date(request):
    book_date_form = BookingDateForm(request.POST)
    if book_date_form.is_valid():
        return redirect('booking-date', beg=book_date_form.data['date_in'], fin=book_date_form.data['date_out'])
    return redirect('booking')


def booking_successful(request):
    return render(request, 'website/booking-successful.html')


# Администратор
def admin(request):
    if request.user.is_authenticated:
        seasons = Season.objects.all().order_by('start_date')
        reviews_new_dot = get_reviews_new_dot()
        add_season_form = AddSeasonForm()
        price = Price.objects.first()

        update_price_form = UpdatePriceForm()
        update_price_form.fields['double_room'].widget.attrs['placeholder'] = price.double_room
        update_price_form.fields['extra_guest'].widget.attrs['placeholder'] = price.extra_guest
        return render(request, 'website/admin/admin-index.html', {
            'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
            'seasons': seasons,
            'add_season_form': add_season_form,
            'update_price_form': update_price_form,
        })
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('admin')
    login_form = LoginForm()
    return render(request, 'website/admin/admin-login.html', {'login_form': login_form, })


@login_required(login_url='/admin')
def admin_logout(request):
    logout(request)
    return redirect('admin')


@login_required(login_url='/admin')
def admin_reviews(request):
    reviews_new_len = len(Review.objects.filter(status=0))
    if reviews_new_len > 0:
        return redirect('admin-reviews-new')
    return redirect('admin-reviews-public')


@login_required(login_url='/admin')
def admin_reviews_new(request):
    reviews_new = Review.objects.filter(status=0)
    reviews_new_dot = get_reviews_new_dot()
    return render(request, 'website/admin/admin-reviews-new.html', {
        'reviews_new': reviews_new,
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
    })


@login_required(login_url='/admin')
def admin_reviews_public(request):
    reviews_public = Review.objects.filter(status=1)
    reviews_new_dot = get_reviews_new_dot()
    return render(request, 'website/admin/admin-reviews-public.html', {
        'reviews_public': reviews_public,
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
    })


@login_required(login_url='/admin')
def admin_reviews_publish(request, pk):
    review = get_object_or_404(Review, id=pk)
    review.status = 1
    review.save()
    return redirect('admin-reviews-public')


@login_required(login_url='/admin')
def admin_reviews_archive(request):
    reviews_trash = Review.objects.filter(status=2)
    reviews_new_dot = get_reviews_new_dot()
    return render(request, 'website/admin/admin-reviews-trash.html', {
        'reviews_trash': reviews_trash,
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
    })


@login_required(login_url='/admin')
def admin_reviews_archivate(request, pk):
    review = get_object_or_404(Review, id=pk)
    review.status = 2
    review.save()
    return redirect('admin-reviews')


@login_required(login_url='/admin')
def admin_reviews_delete(request, pk):
    review = get_object_or_404(Review, id=pk)
    review.delete()
    return redirect('admin-reviews-archive')


@login_required(login_url='/admin')
def admin_gallery(request, url=None):
    if url is not None:
        galleries = Gallery.objects.all()
        urls = []

        current_gallery = None
        for gallery in galleries:
            urls.append(gallery.url)
            if gallery.url == url:
                current_gallery = gallery

        if request.method == 'POST':  # загрузка фото в определенную вкладку
            photo_upload_form = PhotoUploadForm(request.POST, request.FILES)
            if photo_upload_form.is_valid():
                photo = photo_upload_form.save()
                gallery_photo = GalleryPhoto.objects.create(photo=photo, gallery=current_gallery)

        gallery_photos = GalleryPhoto.objects.filter(gallery=current_gallery)
        photos = []
        for gallery_photo in gallery_photos:
            photos.append(gallery_photo.photo)

        if url in urls:
            reviews_new_dot = get_reviews_new_dot()
            photo_upload_form = PhotoUploadForm()
            return render(request, 'website/admin/admin-gallery.html', {
                'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
                'galleries': galleries,
                'current_gallery': current_gallery,
                'photos': photos,
                'photo_upload_form': photo_upload_form,
            })

    first_gallery = Gallery.objects.first()
    return redirect('admin-gallery-open', url=first_gallery.url)


@login_required(login_url='/admin')
def admin_photo_delete(request, pk):
    photo = get_object_or_404(Photo, pk=pk)
    photo.delete()
    return redirect('admin')


@login_required(login_url='/admin')
def admin_seasons_add(request):
    if request.method == 'POST':
        add_season_form = AddSeasonForm(request.POST)
        if add_season_form.is_valid():
            season = add_season_form.save(commit=False)
            if season.start_date < datetime.now().date():
                season.start_date = datetime(season.start_date.year + 1, season.start_date.month, season.start_date.day)
            season.save()
    return redirect('admin')


@login_required(login_url='/admin')
def admin_seasons_delete(request, pk):
    season = get_object_or_404(Season, pk=pk)
    season.delete()
    return redirect('admin')


@login_required(login_url='/admin')
def admin_change_prices(request):
    if request.method == 'POST':
        update_price_form = UpdatePriceForm(request.POST)
        price = Price.objects.first()

        if update_price_form['double_room'].value() != '':
            price.double_room = update_price_form['double_room'].value()

        if update_price_form['extra_guest'].value() != '':
            price.extra_guest = update_price_form['extra_guest'].value()

        price.save()

    return redirect('admin')


@login_required(login_url='/admin')
def admin_rtypes(request):
    reviews_new_dot = get_reviews_new_dot()

    r_types = RType.objects.all()
    rtypes = []
    for r_type in r_types:  # тип номера и первое изображение для типа
        r_type_photo_first = RTypePhoto.objects.filter(r_type=r_type).first()
        price_low = RTypeSeason.objects.filter(r_type=r_type).order_by('price').first()
        if price_low is not None:
            price_low = price_low.price
        rtypes.append([r_type, r_type_photo_first, price_low])
    return render(request, 'website/admin/admin-rtypes.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'rtypes': rtypes,
    })


@login_required(login_url='/admin')
def admin_rtype(request, url=None):
    reviews_new_dot = get_reviews_new_dot()

    r_type = get_object_or_404(RType, url=url)
    r_type_photos = RTypePhoto.objects.filter(r_type=r_type)

    r_type_equipments = RTypeEquipment.objects.filter(r_type=r_type)

    price = Price.objects.first()
    price_current = RTypeSeason.objects.filter(r_type=r_type).order_by('season__start_date').first().price
    price_current_two = price_current + price.double_room
    price_extra_guest = price.extra_guest

    add_rtype_form = AddRTypeForm()
    return render(request, 'website/admin/admin-rtype.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'r_type': r_type,
        'r_type_photos': r_type_photos,
        'price_current': price_current,
        'price_current_two': price_current_two,
        'price_extra_guest': price_extra_guest,
        'r_type_equipments': r_type_equipments,
        'add_rtype_form': add_rtype_form,
    })


@login_required(login_url='/admin')
def admin_rtypes_add(request):
    reviews_new_dot = get_reviews_new_dot()

    add_rtype_form = AddRTypeForm()
    if request.method == 'POST':
        add_rtype_form = AddRTypeForm(request.POST, request.FILES)
        photos = request.FILES.getlist('photos')
        seasons = Season.objects.order_by('start_date')
        equipments = Equipment.objects.all()

        if add_rtype_form.is_valid():
            rtype = add_rtype_form.save()
            for photo in photos:
                new_photo = Photo(image=photo)
                new_photo.save()
                new_rtype_photo = RTypePhoto(r_type=rtype, photo=new_photo)
                new_rtype_photo.save()

            for field in add_rtype_form.fields:  # для динамических полей
                if field.startswith('season_'):
                    rtype_season = RTypeSeason(season=seasons.get(pk=field.replace('season_', '')), r_type=rtype,
                                               price=request.POST.get(field))
                    rtype_season.save()

                if field.startswith('equipment_'):
                    rtype_equipment = RTypeEquipment(equipment=equipments.get(pk=field.replace('equipment_', '')),
                                                     r_type=rtype)
                    rtype_equipment.save()

            return redirect('admin-rtypes')  # необходимо, что бы не создавалась повторная запись при обновлении

    return render(request, 'website/admin/admin-rtype-add.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'add_rtype_form': add_rtype_form,
    })


@login_required(login_url='/admin')
def admin_rooms(request):
    reviews_new_dot = get_reviews_new_dot()

    bookings_full = Booking.objects.filter(is_over=False).order_by('date_out')

    rooms_free = list(Room.objects.all().order_by('url'))
    for booking in bookings_full:
        if booking.room in rooms_free:
            rooms_free.remove(booking.room)

    return render(request, 'website/admin/admin-rooms.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'bookings_full': bookings_full,
        'rooms_free': rooms_free,
    })


@login_required(login_url='/admin')
def admin_room_add(request):
    reviews_new_dot = get_reviews_new_dot()

    add_room_form = AddRoomForm()

    if request.method == 'POST':
        add_room_form = AddRoomForm(request.POST, request.FILES)
        photos = request.FILES.getlist('photos')

        if add_room_form.is_valid():
            room = add_room_form.save(commit=False)
            if len(photos) > 0:
                room.show = True
            room.save()

            return redirect('admin-rooms')  # необходимо, что бы не создавалась повторная запись при обновлении
    return render(request, 'website/admin/admin-rooms-add.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'add_rtype_form': add_room_form,
    })


@login_required(login_url='/admin')
def admin_room(request, url=None, url2=None):
    reviews_new_dot = get_reviews_new_dot()

    room = get_object_or_404(Room, url=url)
    bookings = Booking.objects.filter(is_over=False, room=room)

    if request.method == 'POST':
        edit_room_form = EditRoomForm(request.POST)

        if edit_room_form.is_valid():
            room = edit_room_form.save()
            room.save()

    edit_room_form = EditRoomForm(
        initial={'url': room.url, 'title': room.title, 'info': room.info, 'r_type': room.r_type})
    return render(request, 'website/admin/admin-room.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'room': room,
        'edit_room_form': edit_room_form,
        'bookings': bookings,
    })


@login_required(login_url='/admin')
def admin_guests(request):
    reviews_new_dot = get_reviews_new_dot()

    guests = Guest.objects.all().order_by('name')

    return render(request, 'website/admin/admin-guests.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'guests': guests,
    })


@login_required(login_url='/admin')
def admin_guest(request, url=None):
    reviews_new_dot = get_reviews_new_dot()

    guest = get_object_or_404(Guest, id=url)
    open_bookings_for_guest = Booking.objects.filter(guest=guest, is_over=False)

    edit_guest_form = EditGuestForm(initial={'name': guest.name, 'phone': guest.phone, 'passport': guest.passport})
    add_booking_form = AddBookingForm()
    return render(request, 'website/admin/admin-guest.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'guest': guest,
        'edit_guest_form': edit_guest_form,
        'add_booking_form': add_booking_form,
        'open_bookings_for_guest': open_bookings_for_guest,
    })


@login_required(login_url='/admin')
def admin_guest_checkin(request, url=None):
    add_booking_form = AddBookingForm(request.POST)
    guest = get_object_or_404(Guest, id=url)

    if add_booking_form.is_valid():
        room = Room.objects.get(id=add_booking_form['room'].data)
        booking = add_booking_form.save(commit=False)
        booking.guest = guest
        booking.room = room
        booking.save()
    return redirect('admin-guest', url=url)


@login_required(login_url='/admin')
def admin_guest_add(request):
    reviews_new_dot = get_reviews_new_dot()

    add_guest_form = AddGuestForm(initial={'new': False})
    if request.method == 'POST':
        add_guest_form = AddGuestForm(request.POST, initial={'new': False})

        if add_guest_form.is_valid():
            guest = add_guest_form.save()
            return redirect('admin-guest', url=guest.id)

    return render(request, 'website/admin/admin-guest-add.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'add_guest_form': add_guest_form,
    })


@login_required(login_url='/admin')
def admin_booking_checkout(request, url):
    booking = get_object_or_404(Booking, id=url)
    booking.is_over = True
    booking.save()
    return redirect('admin-guest', url=booking.guest.id)


@login_required(login_url='/admin')
def admin_booking_requests(request):
    reviews_new_dot = get_reviews_new_dot()

    booking_requests = BookingRequest.objects.all()

    return render(request, 'website/admin/admin-booking-requests.html', {
        'reviews_new_dot': reviews_new_dot, 'booking_request_new_dot': get_reviews_new_dot(),
        'booking_requests': booking_requests,
    })


@login_required(login_url='/admin')
def admin_booking_request_delete(request, id=None):
    if id is not None:
        get_object_or_404(BookingRequest, id=id).delete()
    return redirect('admin-booking-requests')


@login_required(login_url='/admin')
def admin_messages(request):
    messages = IndexMessage.objects.all()

    return render(request, 'website/admin/admin-messages.html', {
        'reviews_new_dot': get_reviews_new_dot(), 'booking_request_new_dot': get_reviews_new_dot(),
        'messages': messages,
    })
