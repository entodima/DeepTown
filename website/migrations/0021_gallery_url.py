# Generated by Django 2.2 on 2019-04-06 12:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('website', '0020_auto_20190406_1304'),
    ]

    operations = [
        migrations.AddField(
            model_name='gallery',
            name='url',
            field=models.CharField(default='', max_length=20),
        ),
    ]
