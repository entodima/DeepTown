let window_width = window.innerWidth;
let window_height = window.innerHeight;
let rightPanel = undefined;

var rows = undefined

//добавление/удаление класса phone
function addOrRemovePhone() {
    rightPanel = $("#right-panel");
    window_width = window.innerWidth;
    window_height = window.innerHeight;
    let body = document.getElementById('body');
    if (window_width <= 640) {
        $(body).addClass('phone');
        $(body).removeClass('desktop tablet');
        $(rightPanel).addClass('hided');
        $(rightPanel).hide();
    }
    else {
        if (window_width >= 1000) {
            $(body).removeClass('phone');
            $(body).removeClass('tablet');
            $(body).addClass('desktop')
        } else {
            $(body).removeClass('phone desktop');
            $(body).addClass('tablet')
        }

        $(rightPanel).removeClass('hided');
        $(rightPanel).show();
    }
    if(window_height <= 600){
        $('#right-panel-contact').addClass('hided');
    } else {
        $('#right-panel-contact').removeClass('hided');
    }
}
window.addEventListener('resize',function () {
    addOrRemovePhone()
    if (window_width>680){
        index_show_photos();
    }
    else {
        index_hide_photos();
    }
});


const animationSpeed = 250;
$(document).ready(function () {
    //открытие/закрытие гамбургера
    rightPanel = $("#right-panel");
    $("#show-right-menu-button").click(function () {
        if ($(rightPanel).hasClass('hided')) {
            $(rightPanel).fadeIn(animationSpeed).queue(function () {
                $(rightPanel).removeClass('hided');
                $(this).dequeue();
            });
        } else {
            $(rightPanel).fadeOut(animationSpeed).queue(function () {
                $(rightPanel).addClass('hided');
                $(this).dequeue();
            });
        }
        this.classList.toggle("is-active");
    });

    //отзывы - открытие формы
    $('#show-review-send-form').click(function () {
        $(this).hide();
        $('#review-send-form').show();
        $('#review-send-form-textarea').focus();
    })

    rows = $('.index-row');
    scroll();
    image_def = $('#index-image-img');
    if (window_width>680){
        index_show_photos();
    }
    else {
        index_hide_photos();
    }
});

//добавление подчеркивания в правом меню
function auto_underline(){
    let url_splitted = document.URL.split('/');
    if (url_splitted[3] !== "") {
        let count = document.getElementById('right-panel').childNodes;

        count.forEach(function (element) {
            if(document.URL.includes($(element).attr('href'))){
                let panel_url_splitted = $(element).attr('href').split('/');
                if(panel_url_splitted[1] === url_splitted[3]){
                    $(element).addClass('right-panel-url-selected');
                }
            }
        });
    }
}

//изменение прозрачности в галерее для заголовка
function gallery_auto_opacity() {
    let count = document.getElementById('horizontal-scroll').childNodes;
    count.forEach(function (element) {
        if (document.URL.includes($(element).attr('href'))){
            $(element).attr('style','opacity=1')
        }
    })
}

//показать добавление класса номера
function admin_rtypes_add_show() {
   $('#admin-rtypes-show').hide();
   $('#admin-rtypes-add-div').show();
}

function booking_select(id) {
    rows = document.getElementsByClassName('booking-table-row');

    [].forEach.call(rows,function (row) {
        if(row.id === 'rtype'+id)
        $(row).addClass('active-rtype');
    })
}


//автосворачивание верхней панели
let prevScrollpos = window.pageYOffset;

window.onscroll = function() {scroll()};
image_def = undefined;
function scroll(){
    let currentScrollPos = window.pageYOffset;
    if (prevScrollpos > currentScrollPos || currentScrollPos < 16) {
    document.getElementById("header").style.top = "0";
      } else {
        document.getElementById("header").style.top = "-60px";
      }
      prevScrollpos = currentScrollPos;

//анимация на главной странице
    Array.prototype.forEach.call(rows, function(row) {
        //console.log(row.getBoundingClientRect().top)
        if(row.getBoundingClientRect().top <= 68){
            $(row.children[0].children).fadeOut(400).queue(function () {
                if($(row.children[0].children).attr('src') != $(image_def[0]).attr('src')){
                    $(image_def[0]).fadeOut(200).queue(function () {
                    $(image_def[0]).attr('src', $(row.children[0].children).attr('src'))
                    $(image_def[0]).fadeIn(200)
                    $(this).dequeue()
                })
                }
                $(this).dequeue()
            })

        }
        if(row.getBoundingClientRect().top > 68){
            $(row.children[0].children).fadeIn(400)
        }
    });
}
function index_hide_photos() {
    left_rows = $('.index-leftside')
    Array.prototype.forEach.call(left_rows, function(row) {
        $(row).hide();
    })
}
function index_show_photos() {
    left_rows = $('.index-leftside')
    Array.prototype.forEach.call(left_rows, function(row) {
        $(row).show()
    })
}