function validate_review(){
    const text = document.getElementById('review-send-form-textarea');
    const author = document.getElementById('review-send-form-author');

    if (text.value.length>0 && author.value.length>0){
        $('#review-send-submit').show()
    } else {
        $('#review-send-submit').hide()
    }
}