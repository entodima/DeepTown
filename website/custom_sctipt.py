from .models import Review, BookingRequest


def get_reviews_new_dot():
	reviews_new_dot = Review.objects.filter(status=0).count()
	if reviews_new_dot > 0:
		reviews_new_dot = ' (' + str(reviews_new_dot) + ')'
	else:
		reviews_new_dot = ''
	return reviews_new_dot

def get_booking_request_dot():
	booking_request_dot = BookingRequest.objects.all().count()
	if booking_request_dot > 0:
		booking_request_dot = ' (' + str(booking_request_dot) + ')'
	else:
		booking_request_dot = ''
	return booking_request_dot