from django.contrib import admin
from .models import *


# Register your models here.
class ReviewAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'status', )


class GalleryAdmin(admin.ModelAdmin):
    list_display = ('title', 'url')


class RTypeAdmin(admin.ModelAdmin):
    list_display = ('title', 'url' )


class EquipmentAdmin(admin.ModelAdmin):
    list_display = ('title', 'has_icon')
    def has_icon(self, obj):
        if obj.icon == '':
            return False
        else:
            return True
    has_icon.short_description = 'С иконкой?'
    has_icon.boolean = True


class RoomAdmin(admin.ModelAdmin):
    list_display = ('title', 'get_rtype_title', 'url', 'show',)  # TODO: вывод ссылки на тип

    def get_rtype_title(self, obj):
        return obj.r_type.title
    get_rtype_title.short_description = 'Класс номера'


class GalleryPhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_gallery_title', 'photo')  # TODO: вывод ссылки на фото

    def get_gallery_title(self, obj):
        return obj.gallery.title
    get_gallery_title.short_description = 'Галерея'
    get_gallery_title.admin_order_field = '-gallery__id'


class PromoAdmin(admin.ModelAdmin):
    list_display= ('title', 'text', )


class RTypeEquipmentAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_equipment_title', 'get_rtype_title', )  # TODO: вывод ссылки

    def get_rtype_title(self, obj):
        return obj.r_type.title

    def get_equipment_title(self, obj):
        return obj.equipment.title


class RoomPhotoAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_room_title', 'get_room_rtype_title', 'photo')  # TODO: вывод ссылки

    def get_room_title(self, obj):
        return obj.room.title

    def get_room_rtype_title(self, obj):
        return obj.room.r_type.title

    get_room_title.short_description = 'Название номера'
    get_room_rtype_title.short_description = 'Название класса'


class RTypePhotoAdmin(admin.ModelAdmin):  # TODO: вывод ссылки
    list_display = ('id', 'get_rtype_title')

    def get_rtype_title(self, obj):
        return obj.r_type.title


class SeasonAdmin(admin.ModelAdmin):
    list_display = ('title', 'start_date')


class RTypeSeasonAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_rtype_title', 'get_season_title', 'price')  # TODO: вывод ссылки

    def get_rtype_title(self, obj):
        return obj.r_type.title

    def get_season_title(self, obj):
        return obj.season.title

    get_rtype_title.short_description = 'Название класса'
    get_season_title.short_description = 'Название сезона'


admin.site.register(Promo, PromoAdmin)
admin.site.register(Review, ReviewAdmin)
admin.site.register(Gallery, GalleryAdmin)
admin.site.register(GalleryPhoto, GalleryPhotoAdmin)
admin.site.register(RType, RTypeAdmin)
admin.site.register(RTypePhoto, RTypePhotoAdmin)
admin.site.register(Equipment, EquipmentAdmin)
admin.site.register(RTypeEquipment, RTypeEquipmentAdmin)
admin.site.register(Room, RoomAdmin)
admin.site.register(Season, SeasonAdmin)
admin.site.register(RTypeSeason, RTypeSeasonAdmin)
admin.site.register(Price)
admin.site.register(Photo)
admin.site.register(Service)
admin.site.register(ServicePhoto)
admin.site.register(Booking)
admin.site.register(Guest)
admin.site.register(BookingRequest)
admin.site.register(IndexMessage)
