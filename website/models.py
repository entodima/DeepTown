from django.db import models


class Promo(models.Model):
	title = models.CharField(max_length=30)
	text = models.TextField(null=True, max_length=400)


class Review(models.Model):
	author = models.CharField(max_length=30)
	text = models.TextField()
	status = models.IntegerField(default=0)


class Photo(models.Model):
	image = models.ImageField(upload_to='photos', blank=True, null=False)


class Gallery(models.Model):
	title = models.CharField(max_length=20, unique=True, null=False)
	url = models.CharField(max_length=20, unique=True, null=False)


class GalleryPhoto(models.Model):
	gallery = models.ForeignKey('Gallery', on_delete=models.CASCADE)
	photo = models.ForeignKey('Photo', on_delete=models.CASCADE)

	class Meta:
		unique_together = ('gallery', 'photo')


class RType(models.Model):
	url = models.CharField(max_length=20, unique=True)
	title = models.CharField(max_length=20, unique=True)
	max_capacity = models.IntegerField()
	area = models.CharField(max_length=10)
	info = models.TextField()

	def __str__(self):
		return self.title  # вывод заголовка в форме добавления номера


class RTypePhoto(models.Model):
	r_type = models.ForeignKey('RType', on_delete=models.CASCADE)
	photo = models.ForeignKey('Photo', on_delete=models.CASCADE)


class Equipment(models.Model):
	title = models.CharField(max_length=20, unique=True)
	icon = models.ImageField(upload_to='equipments', blank=True, null=True)


class RTypeEquipment(models.Model):
	r_type = models.ForeignKey('RType', on_delete=models.CASCADE)
	equipment = models.ForeignKey('Equipment', on_delete=models.CASCADE)


class Room(models.Model):
	r_type = models.ForeignKey('RType', on_delete=models.CASCADE)
	url = models.CharField(max_length=20, unique=True)
	title = models.CharField(max_length=20, unique=True)
	info = models.TextField(null=True)

	show = models.BooleanField(null=False, default=False)
	def __str__(self):
		return self.title  # вывод заголовка в форме добавления номера


class Season(models.Model):
	title = models.CharField(max_length=40)
	start_date = models.DateField()


class RTypeSeason(models.Model):
	r_type = models.ForeignKey('RType', on_delete=models.CASCADE)
	season = models.ForeignKey('Season', on_delete=models.CASCADE)
	price = models.IntegerField()


class Price(models.Model):
	double_room = models.IntegerField()
	extra_guest = models.IntegerField()


class Service(models.Model):
	title = models.CharField(max_length=40)


class ServicePhoto(models.Model):
	service = models.ForeignKey('Service', on_delete=models.CASCADE)
	photo = models.ForeignKey('Photo', on_delete=models.CASCADE)


class Guest(models.Model):
	name = models.CharField(max_length=50)
	phone = models.IntegerField(unique=True)
	passport = models.TextField(max_length=400)

	def __str__(self):
		return self.name  # вывод заголовка в форме добавления номера


class Booking(models.Model):
	date_in = models.DateField()
	date_out = models.DateField()
	peoples = models.IntegerField()
	guest = models.ForeignKey('Guest', on_delete=models.CASCADE)
	room = models.ForeignKey('Room', on_delete=models.CASCADE)
	is_over = models.BooleanField(default=False)


class BookingRequest(models.Model):
	date_in = models.DateField()
	date_out = models.DateField()
	peoples = models.IntegerField()
	name = models.CharField(max_length=50)
	phone = models.IntegerField(unique=False)
	rtype = models.ForeignKey(RType, on_delete=models.CASCADE)


class IndexMessage(models.Model):
	title = models.CharField(max_length=15)
	text = models.TextField()
	image = models.ImageField(upload_to='photos', blank=True, null=False)
