from django import  forms
from django.forms import ModelChoiceField
from .models import Review, Gallery, Photo, Season, Price, RType, Equipment, Room, Guest, Booking, BookingRequest
import datetime


class ReviewSendForm(forms.ModelForm):
	author = forms.Field(label='', widget=forms.TextInput(attrs={
		'id': 'review-send-form-author',
		'placeholder': 'ваше имя',
		'onkeyup': 'validate_review()'}))
	text = forms.Field(label='', widget=forms.Textarea(attrs={
		'id': 'review-send-form-textarea',
		'placeholder': 'РАССКАЖИТЕ НАМ ЧТО ВЫ ДУМАЕТЕ О НАС, МЫ ОБЯЗАТЕЛЬНО УЧТЕМ ВСЕ ВАШИ ПОЖЕЛАНИЯ',
		'rows': '4',
		'cols': 'auto',
		'onkeyup': 'validate_review()'}))

	class Meta:
		model = Review
		fields = ('text', 'author',)


class LoginForm(forms.Form):
	username = forms.Field(label='', widget=forms.TextInput(attrs={
		'placeholder': 'Login',
	}))
	password = forms.Field(label='', widget=forms.PasswordInput(attrs={
		'placeholder': 'Password',
	}))


class PhotoUploadForm(forms.ModelForm):
	image = forms.ImageField(label='', widget=forms.FileInput(attrs={'onchange': 'submit();'}))

	class Meta:
		model = Photo
		fields = ('image', )


class AddSeasonForm(forms.ModelForm):

	title = forms.Field(label='', widget=forms.TextInput(attrs={
		'type': 'text',
		'maxlength': '40',
		'placeholder': 'Новый сезон', }))
	start_date = forms.Field(label='', widget=forms.DateInput(attrs={'type': 'date'}))

	class Meta:
		model = Season
		fields = ('title', 'start_date', )


class UpdatePriceForm(forms.Form):
	double_room = forms.IntegerField(label='', widget=forms.TextInput(
		attrs={
			'onchange': 'document.getElementById("admin-prices-form").submit()',
			'value': '',
		}))
	extra_guest = forms.IntegerField(label='', widget=forms.TextInput(
		attrs={
			'onchange': 'document.getElementById("admin-prices-form").submit()',
			'value': '',
		}))


class AddRTypeForm(forms.ModelForm):
	url = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Ссылка'}))
	title = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Название класса'}))
	max_capacity = forms.IntegerField(label='', widget=forms.TextInput(attrs={'placeholder': 'Вместимость'}))
	area = forms.IntegerField(label='', widget=forms.TextInput(attrs={'placeholder': 'Площадь'}))
	info = forms.Field(label='', widget=forms.Textarea(attrs={'placeholder': 'Описание класса', 'rows': '4'}))

	photos = forms.FileField(label='', widget=forms.ClearableFileInput(attrs={'multiple': True}), required=True)

	class Meta:
		model = RType
		fields = ('title', 'url', 'max_capacity', 'area', 'info', )

	def __init__(self, *args, **kwargs):
		for season in Season.objects.order_by('start_date'):
			self.base_fields['season_'+str(season.id)] = forms.IntegerField(label='', widget=forms.TextInput(attrs={'placeholder': season.title+' - стоимость', }))

		for equipment in Equipment.objects.all():
			self.base_fields['equipment_'+str(equipment.id)] = forms.BooleanField(label=equipment.title, required=False, widget=forms.CheckboxInput())
		super(AddRTypeForm, self).__init__(*args, **kwargs)


class AddRoomForm(forms.ModelForm):
	url = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Ссылка'}))
	title = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Название номера'}))
	info = forms.Field(label='', widget=forms.Textarea(attrs={'placeholder': 'Описание этого номера', 'rows': '4'}))
	r_type = forms.ModelChoiceField(label='', initial=None,
									queryset=RType.objects.all(), empty_label='Класс номера')

	class Meta:
		model = Room
		fields = ('title', 'url',  'info', 'r_type')


class EditRoomForm(AddRoomForm):
	class Meta:
		model = Room
		fields = ('title', 'url',  'info', 'r_type')


class AddGuestForm(forms.ModelForm):
	name = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Имя'}))
	phone = forms.Field(label='', widget=forms.NumberInput(attrs={'placeholder': 'Номер'}))
	passport = forms.Field(label='', widget=forms.Textarea(attrs={'placeholder': 'Паспортные данные', 'rows': '4'}))

	class Meta:
		model=Guest
		fields = ('name', 'phone',  'passport',)


class EditGuestForm(AddGuestForm):
	class Meta:
		model=Guest
		fields = ('name', 'phone',  'passport',)


class AddBookingForm(forms.ModelForm):
	date_in = forms.DateField(label='', widget=forms.DateInput(attrs={'placeholder': 'Дата заезда', 'type': 'date','value': datetime.date.today()}))
	date_out = forms.DateField(label='', widget=forms.DateInput(attrs={'placeholder': 'Дата выезда', 'type': 'date','value': datetime.date.today()}))
	peoples = forms.IntegerField(label='',widget=forms.TextInput(attrs={'placeholder': 'Количество человек'}))
	room = forms.ModelChoiceField(label='', queryset=Room.objects.all(), empty_label='Номер')

	class Meta:
		model=Booking
		fields = ('date_in', 'date_out',  'peoples',)


class BookingDateForm(forms.Form):
	date_in = forms.DateField(label='', widget=forms.DateInput(attrs={'placeholder': 'Дата заезда', 'type': 'date','value': datetime.date.today(), 'onchange': 'document.getElementById("booking-search-form").submit();'}))
	date_out = forms.DateField(label='', widget=forms.DateInput(attrs={'placeholder': 'Дата выезда', 'type': 'date','value': datetime.date.today(), 'onchange': 'document.getElementById("booking-search-form").submit();'}))


class BookingRequestForm(forms.Form):
	peoples = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Количество человек'}))
	name = forms.Field(label='', widget=forms.TextInput(attrs={'placeholder': 'Имя'}))
	phone = forms.Field(label='', widget=forms.NumberInput(attrs={'placeholder': 'Номер'}))

	class Meta:
		fields = ('peoples', 'name',  'phone', 'passport')