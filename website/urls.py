"""app URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('booking/', views.booking, name='booking'),
    path('promo/', views.promo, name='promo'),
    path('rooms/', views.rooms, name='rooms'),
    path('rooms/<str:url>/', views.rooms, name='rooms'),
    path('rooms/<str:url>/<int:url2>/', views.rooms, name='rooms-art'),
    path('services/', views.services, name='services'),
    path('prices/', views.prices, name='prices'),
    path('gallery/', views.gallery, name='gallery'),
    path('gallery/<str:url>/', views.gallery, name='gallery-open'),
    path('reviews/', views.reviews, name='reviews'),
    path('booking/', views.booking, name='booking'),
    path('booking/<str:beg>_<str:fin>/', views.booking, name='booking-date'),
    path('booking/<str:beg>_<str:fin>/<int:id>/', views.booking, name='booking-date-rtype'),
    path('booking/date/', views.booking_date, name='booking-date-form'),
    path('booking/successful/', views.booking_successful, name='booking-successful'),
    path('contacts/', views.contacts, name='contacts'),

    path('admin/', views.admin, name='admin'),
    path('admin/dash/seasons/add', views.admin_seasons_add, name='admin-season-add'),
    path('admin/dash/seasons/<int:pk>/delete', views.admin_seasons_delete, name='admin-season-delete'),
    path('admin/dash/change-prices', views.admin_change_prices, name='admin-change-prices'),
    path('admin/logout', views.admin_logout, name='admin-logout'),
    path('admin/reviews', views.admin_reviews, name='admin-reviews'),
    path('admin/reviews/new', views.admin_reviews_new, name='admin-reviews-new'),
    path('admin/reviews/archivate/<int:pk>', views.admin_reviews_archivate, name='admin-reviews-archivate'),
    path('admin/reviews/public', views.admin_reviews_public, name='admin-reviews-public'),
    path('admin/reviews/publish/<int:pk>', views.admin_reviews_publish, name='admin-reviews-publish'),
    path('admin/reviews/archive', views.admin_reviews_archive, name='admin-reviews-archive'),
    path('admin/reviews/delete/<int:pk>', views.admin_reviews_delete, name='admin-reviews-delete'),
    path('admin/gallery/', views.admin_gallery, name='admin-gallery'),
    path('admin/gallery/<str:url>', views.admin_gallery, name='admin-gallery-open'),
    path('admin/photo/<int:pk>/delete', views.admin_photo_delete, name='admin-photo-delete'),
    path('admin/rtypes/', views.admin_rtypes, name='admin-rtypes'),
    path('admin/rtypes/<str:url>/',views.admin_rtype, name='admin-rtype'),
    path('admin/rtypes-add/', views.admin_rtypes_add, name='admin-rtype-add'),
    path('admin/rooms/', views.admin_rooms, name='admin-rooms'),
    path('admin/rooms/<str:url>/', views.admin_room, name='admin-room'),
    path('admin/rooms/<str:url>/del-photo/<int:url2>', views.admin_room, name='admin-room'),
    path('admin/rooms-add/', views.admin_room_add, name='admin-rooms-add'),
    path('admin/guests/', views.admin_guests, name='admin-guests'),
    path('admin/guests/<int:beg>-<int:fin>/', views.admin_guests, name='admin-guests'),
    path('admin/guests/<str:url>/', views.admin_guest, name='admin-guest'),
    path('admin/guests/<str:url>/checkin', views.admin_guest_checkin, name='admin-guest-checkin'),
    path('admin/guests-add/', views.admin_guest_add, name='admin-guest-add'),
    path('admin/bookings/<int:url>/checkout', views.admin_booking_checkout, name='admin_booking_checkout'),
    path('admin/bookings_requests/', views.admin_booking_requests, name='admin-booking-requests'),
    path('admin/bookings_requests/<int:id>/delete/', views.admin_booking_request_delete, name='admin-booking-request-delete'),
    path('admin/messages/', views.admin_messages, name='admin-messages'),
]
