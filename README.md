# DeepTown

В данном репозитории хранится курсовая работа по дисциплне "Веб-технологии" студента группы ИС/б-31-о СевГУ.

"WEB-сайт для гостинницы Вилла DeepTown."

### Развернутое приложение
https://eatmornic.pythonanywhere.com

### Запуск приложения
Изменить настройки базы данных в файле ```/app/settings.py``` или поставить SQLite базу:
```python
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
} 
```
Открыть папку в терминале
```bash
cd /.../DeepTown
```

Переключиться в пространство ```venv```:
```bash
source venv/bin/activate
```

Произвести разметку базы данных
```python
python ./manage.py migrate
```

Создать суперпользователя:
```python
python ./manage.py createsuperuser
```

Запустить сервер:
```python
python ./manage.py runserver
```
